<?php
include '../autoloader.php';

class Redirect extends DAO{
    public static function targetRedirect(){
       if (isset($_POST['accept'])){
            header("Location:/site/add-product.php?error=none");
            exit;
        }else if (isset($_POST['decline'])){
            $delete = new DAO();
            $delete->Delete();
        }else{
            header("Location:/site/index.php?error=something-wrong");
            exit;
        } 
    }  
}
Redirect::targetRedirect();
?>