<?php
include '../autoloader.php';

class ProductPOST{
        function __construct($selector,$SKU,$name,$price,$size,$weight,$height,$width,$length) {
                $this->selector = $selector;
                $this->SKU = $SKU;
                $this->name = $name;
                $this->price = $price;
                $this->size = $size;
                $this->weight = $weight;
                $this->height = $height;
                $this->width = $width;
                $this->length = $length;
        }
        function POST(){
                //DVD,Book or Furniture value from selector
                $getItem = $this->selector;
                //POST items
                $validator = new $getItem($this->selector,$this->SKU,$this->name,$this->price,$this->size,$this->weight,$this->height,$this->width,$this->length);
                //PostItem is located in DVD,Book and Furniture classes
                $validator->postItem();
                }    
        }

$AddRedirect = new ProductPOST($_POST["productType"],$_POST["sku"],$_POST["name"],$_POST["price"],$_POST["size"],$_POST["weight"],$_POST["height"],$_POST["width"],$_POST["length"]);
$AddRedirect->POST();
?>