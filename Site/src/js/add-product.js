  //Show selector value when page opened
    $('#DVDHolder').show();
    $('#BookHolder').hide();
    $('#FurnitureHolder').hide();
  //Hide target div
  $('#productType').change(function(){
    $('#DVDHolder').hide();
    $('#BookHolder').hide();
    $('#FurnitureHolder').hide();
    let object = $(this).val();
    $('#' + object + "Holder").show();
  });
////////////////////////////////////////
//Check if inputs are empty
$(".mandatoryInput").focusout(function(e) {
  let parentID = $(this).parent().attr('id');
  parentID = "#" + parentID;
  if ($.trim(this.value) === "" &&  $(parentID).children("#checkInput").length === 0){
    $(parentID).append("<P style = 'display:inline-block;color:red;' id = 'checkInput'>This input is mandatory to submit.</P>");  
  }else if ($.trim(this.value) === "" &&  $(parentID).children("#checkInput").length === 1){
    return;
  }else{
    $(parentID).children("#checkInput").remove();
  }
  e.stopPropagation();
});
///////////////////////////////////////
//Check if inputs that need to contain numbers is valid
$(".mandatoryNumberInput").focusout(function(e){
  let parentID = $(this).parent().attr('id');
  parentID = "#" + parentID;
  if (/^[0-9.,]+$/.test(this.value) || $.trim(this.value) === ""){
    $(parentID).children("#checkNumberInput").remove();
  }else if($(parentID).children("#checkNumberInput").length === 1){
    return;
  }else{
    $(parentID).append("<P style = 'display:inline-block;color:red;' id = 'checkNumberInput'>This input may contain only numbers.</P>");
  }
});
///////////////////////////////////////
//Check for empty inputs again and POST datas
$('#Save').click(function(event){
  event.preventDefault();
  let value = $("#productType").val();
  let parentDiv = '#' + value + "Holder";
  let input = $(parentDiv).children(".mandatoryInput");
  let valid = true;
  if (!$('#sku').val() || !$('#name').val() || !$('#price').val() || !$(input).val() || !/^[0-9.,]+$/.test($('#price').val())){
      $(".mandatoryInput").focusout();
      return;
  }
  $(input).each(function() {
    if (!/^[0-9.,]+$/.test($(this).val())){
      console.log($(this).val());
      valid = false;
      return false;
    }
  });
  if (valid == true){
    var serializedData = $('#product_form').serialize();
      $.post('classes/ProductPost.Class.php', serializedData).then(function() {
        window.location.replace("http://localhost/site/index.php");
    });
  }
});
////////////////////////////////////////
//Redirect to index.php if clicked Cancel
$('#Cancel').click(function(){
  window.location.replace("http://localhost/site/index.php");
});