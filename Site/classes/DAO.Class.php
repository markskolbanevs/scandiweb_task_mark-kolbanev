<?php
class DAO extends Config{
    public function Select(){
        $sql = "SELECT * FROM products";
        $mysql = $this->connect();
        $query_run = mysqli_query($mysql,$sql);
        if (mysqli_num_rows($query_run) > 0) {
        foreach($query_run as $row){
        ?>
            <div class = 'inputs'>
            <input type='checkbox' class = 'delete-checkbox' name = 'delete-checkbox[]' value = "<?= $row['ID']; ?>" ></input>
            <div class = 'text-holder'>
            <p> SKU: <?= $row['SKU']; ?> </p>
            <p> Name: <?= $row['name']; ?> </p>
            <p> Price: <?= $row['price']; ?> $ </p>
        <?php
            if ($row['object'] == "DVD"){
                echo "<p> Size: ".$row['parameters']." MB</p>";   
            }else if ($row['object'] == "Book"){
                echo "<p> Weight: ".$row['parameters']." KG</p>";   
            }else if ($row['object'] == "Furniture"){
                echo "<p> Dimension: ".$row['parameters']." CM</p>";   
            }
        ?>
        </div>
    </div>
    <?php
            }
        }
    }
    public function Delete(){
        if (isset($_POST['delete-checkbox'])){
            foreach($_POST['delete-checkbox'] as $deleteid){
                $sql = "DELETE FROM products WHERE ID=".$deleteid;
                $mysql = $this->connect();
                mysqli_query($mysql,$sql);
            }
            header("Location:/site/index.php?");
            exit;
        }else{
            header("Location:/site/index.php?");
            exit;  
        } 
    }
    public function ADD($parameter){
        $sql = "INSERT INTO products (SKU,name,price,parameters,object) VALUES (?,?,?,?,?);";
        $mysql = $this->connect();
        $stmt = mysqli_stmt_init($mysql);
            if(!mysqli_stmt_prepare($stmt,$sql)){
                header("Location:/site/add-product.php?error=stmtfailed");
                exit();
            }else if (!mysqli_stmt_init($mysql)){
                header("Location:/site/add-product.php?error=stmtinitfailed");
                exit();
            }
        mysqli_stmt_bind_param($stmt,"ssdss",$this->SKU,$this->name,$this->price,$parameter,$this->selector);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        header("Location:/site/index.php?");
        exit;
     }
}
?>