<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="src/css/add-product.css"> 
    <title>Marks Kolbanevs</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>
<body>
    <div class = "main-container">
      <div class = "main">
      <div class = "header"> 
        <H1>Product Add</H1>
        <div class = "header-holder">
       <form id = "product_form">
        <input class="accept-btn" type='button' value = "Save" name = "action" id = "Save"></input>
        <input class="decline-btn" value = "CANCEL" type='button' name = "action" id = "Cancel"></input>
        </div>
      </div>
      <div class="input-main-add" id="input-main-add">
        <div class="input-add-holder" id = "SkuHolder">
          <H2 style="display:inline-block">*SKU: </H2>
          <input class="mandatoryInput" name="sku" id="sku"></input>
        </div>
      <div class="input-add-holder" id = "NameHolder">
        <H2 style="display:inline-block" >*Name: </H2>
        <input class="mandatoryInput" name="name" id="name"></input>
      </div>
      <div class="input-add-holder" id = "priceHolder">
        <H2 style="display:inline-block">*Price($): </H2>
        <input class="mandatoryInput mandatoryNumberInput" name="price" id="price"></input>
      </div>
      <div class="input-add-holder" >
        <H2 style="display:inline-block">Type Switcher: </H2>
        <select name="productType" id="productType">
          <option value="DVD">DVD</option>
          <option value="Book">Book</option>
          <option value="Furniture">Furniture</option>
        </select>
      </div>
      <div id = "DVDHolder">
        <H2 style="display:inline-block">*Size: </H2>
        <input class="mandatoryInput mandatoryNumberInput" name="size" id="size"></input>
        <br>
        <p class = "notif">Please provide size in (MB)</p>
      </div>
      <div id = "BookHolder">
        <H2 style="display:inline-block">*Weight: </H2>
        <input class="mandatoryInput mandatoryNumberInput" name="weight" id="weight"></input>
        <br>
        <p class = "notif">Please provide weight in (KG)</p>
      </div>
      <div id = "FurnitureHolder">
        <H2 style="display:inline-block">*Height (CM): </H2>
        <input class="mandatoryInput mandatoryNumberInput" name="height" id="height"></input>
        <br>
        <H2 style="display:inline-block">*Width (CM): </H2>
        <input class="mandatoryInput mandatoryNumberInput" name="width" id="width"></input>
        <br>
        <H2 style="display:inline-block">*Length (CM): </H2>
        <input class="mandatoryInput mandatoryNumberInput" name="length" id="length"></input>
        <br>
        <p class = "notif">Please provide HxWxL format (Height,Width,Length)</p>
      </div>
    </div>
</form>
  </div> 
  </div>
 <script src="src/js/add-product.js"></script>
</body>
</html>