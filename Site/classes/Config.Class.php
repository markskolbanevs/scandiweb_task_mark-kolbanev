<?php
define('SERVERNAME', 'localhost');
define ("DBUSERNAME", "root");
define ("DBPASSWORD", "");
define ("DBNAME", "web-site");

class Config{
   public $con = null;

   public function connect(){
        $this->con = mysqli_connect(SERVERNAME,DBUSERNAME,DBPASSWORD,DBNAME);
        if($this->con->connect_error){
            echo "Fail".$this->con->connect_error;
        }
        return $this->con;
    }
}
?>

