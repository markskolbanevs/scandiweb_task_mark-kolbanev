<?php
//DVD,Book and Furniture classes extends Items 
abstract class Items extends DAO{
    function __construct($selector,$SKU,$name,$price,$size,$weight,$height,$width,$length){
        $this->selector = $selector;
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->size = $size;
        $this->weight = $weight;
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }
    abstract function postItem();
}
?>